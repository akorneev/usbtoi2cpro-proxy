all: sdk USBtoI2Cpro.dll

# prepare SDK for i2c adapter
# strip 'extern "C"' from SDK header file (codeword is not supported by C compiler)
sdk:
  md SDK
  copy "c:\Program Files\i2ctools\USB-to-I2C Elite\USBDriver\USBtoI2C32.dll" SDK
  copy "c:\Program Files\i2ctools\USB-to-I2C Elite\DLL Examples\VC 6\USBtoI2C32.h" SDK
  copy "c:\Program Files\i2ctools\USB-to-I2C Elite\DLL Examples\VC 6\USBtoI2C32.lib" SDK
  move SDK\USBtoI2C32.h SDK\USBtoI2C32.h.orig
  powershell -Command "Get-Content 'SDK\USBtoI2C32.h.orig' | ForEach-Object { $$_  -replace 'extern \"C\" ', '' } | Set-Content 'SDK\USBtoI2C32.h'"

USBtoI2Cpro.dll: USBtoI2Cpro.c USBtoI2Cpro.def
  cl USBtoI2Cpro.c USBtoI2Cpro.def /FeUSBtoI2Cpro.dll /LD
  del /F USBtoI2Cpro.obj USBtoI2Cpro.obj USBtoI2Cpro.exp USBtoI2Cpro.lib

clean:
  del /F USBtoI2Cpro.dll
  -rmdir /S /Q SDK
