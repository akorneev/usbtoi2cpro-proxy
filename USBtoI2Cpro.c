//#include "windows.h"

#include "SDK/USBtoI2C32.h"
#pragma comment(lib, "SDK/USBtoI2C32.lib")

int           __stdcall w_GetNumberOfDevices()
{
  return GetNumberOfDevices();
}

int           __stdcall w_GetSerialNumbers(int *SerialNumbers)
{
  return GetSerialNumbers(SerialNumbers);
}

int           __stdcall w_SelectBySerialNumber(int dwSerialNumber)
{
  return SelectBySerialNumber(dwSerialNumber);
}

void          __stdcall w_ShutdownProcedure(void)
{
  ShutdownProcedure();
}

int           __stdcall SetI2CFrequency(int frequency)
{
  return I2C_SetFrequency(1000*frequency);
}

int           __stdcall GetI2CFrequency(void)
{
  return I2C_GetFrequency() / 1000;
}

unsigned char __stdcall I2CWrite(unsigned char address, short int nBytes, unsigned char *WriteData, short int SendStop)
{
  return I2C_Write(address, nBytes, WriteData, SendStop);
}

unsigned char __stdcall I2CRead(unsigned char address, short int nBytes, unsigned char *ReadData, short int SendStop)
{
  return I2C_Read(address, nBytes, ReadData, SendStop);
}

unsigned char __stdcall I2CWriteArray(unsigned char address, unsigned char subaddress, short int nBytes, unsigned char *WriteData)
{
  return I2C_WriteArray(address, subaddress, nBytes, WriteData);
}

unsigned char __stdcall I2CReadArray(unsigned char address, unsigned char subaddress, short int nBytes, unsigned char *ReadData)
{
  return I2C_ReadArray(address, subaddress, nBytes, ReadData);
}
