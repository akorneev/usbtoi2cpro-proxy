   USBtoI2Cpro-proxy
   =================

Wrapper dll to allow use of Sergey's HO_PPP_Control_2010 program developed for
"USB-to-I2C Professional" adapter with "USB-to-I2C Elite" adapter.

The HO_PPP_Control_2010.zip is available at:

  https://cms-docdb.cern.ch/cgi-bin/DocDB/ShowDocument?docid=3104
